local colors = require("onenord.colors").load()

require("onenord").setup({
    styles = {
        comments = "NONE",
        strings = "NONE",
        keywords = "NONE",
        functions = "italic",
        variables = "NONE",
        diagnostics = "underline",
    },
    custom_highlights = {
        TSConstant = { fg = colors.orange },
      },
})
require("lualine").setup({ options = { theme = "onenord" } })

vim.cmd([[
    augroup number_color_toggle
        autocmd!
        autocmd InsertEnter * highlight LineNr ctermbg=green guifg=#A3BE8C
        autocmd InsertLeave * highlight LineNr ctermbg=black guifg=#4C566A
    augroup END
]])

vim.cmd([[
    augroup highlight_yank
        autocmd!
        autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=160}
    augroup END
]])
