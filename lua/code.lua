local vim = vim

local ts_servers    = { "python", "javascript", "vue", "rust", "go", "gomod" }
local lsp_servers   = { "pyright", "rust_analyzer", "tsserver", "gopls", "vls", "sumneko_lua" }

require("treesitter-context").setup({
	enable      = true,
	throttle    = true, -- Throttles plugin updates (may improve performance)
})


-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
for _, lsp in pairs(lsp_servers) do
	require("lspconfig")[lsp].setup({
		on_attach = require("mappings").on_attach,
		flags = {
			-- This will be the default in neovim 0.7+
			debounce_text_changes = 150,
		},
	})
end

require("nvim-treesitter.configs").setup({
	ensure_installed    = ts_servers,
	indent              = { enable = true },
	highlight           = { enable = true },

	textobjects = {
		select = {
			enable = true,
			lookahead = true,
			keymaps = {
				-- You can use the capture groups defined in textobjects.scm
				["af"] = "@function.outer",
				["if"] = "@function.inner",
				["ac"] = "@class.outer",
				["ic"] = "@class.inner",
				["ai"] = "@block.outer",
				["ii"] = "@block.inner",
			},
		},

		swap = {
			enable = true,
			swap_next = {
				["<leader>rs"] = "@parameter.inner",
			},
			swap_previous = {
				["<leader>rS"] = "@parameter.inner",
			},
		},
	},
})
