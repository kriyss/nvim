local vim = vim

vim.g.mapleader = " "
vim.g.maplocalleader = ","

local opts = { noremap = true, silent = true }

-- Other
vim.keymap.set("n", "<leader>c",    ":nohlsearch<cr>",                  opts) -- Clear search
vim.keymap.set("n", "<leader>rc",   ":%s/<C-r><C-w>//g<Left><Left>",    opts) -- Rename
vim.keymap.set("n", "<C-s>",        ":wa<CR>",                          opts) -- Save
vim.keymap.set("i", "<C-s>",        "<Esc>:wa<CR>a",                    opts) -- Save

vim.keymap.set("n", "<leader>j",        "yi[:!google-chrome-stable https://backmarket.atlassian.net/browse/<C-R>0&<CR>", opts)
vim.keymap.set("n", "<localleader>gs",  "yiw:!google-chrome-stable \"https://github.com/search?q=org:BackMarket+<C-R>0&type=issues\"<CR>", opts)



-- Diagnostic
vim.keymap.set("n", "<localleader>d", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
vim.keymap.set("n", "<localleader><localleader>d", vim.diagnostic.setloclist, opts)

-- Invert ; and : to have faster command
vim.keymap.set("n", ";", ":", { noremap = true })
vim.keymap.set("n", ":", ";", { noremap = true })

-- Move code faster
vim.keymap.set("n", "<A-j>", ":m .+1<CR>==",        opts)
vim.keymap.set("n", "<A-k>", ":m .-2<CR>==",        opts)
vim.keymap.set("n", "<A-l>", ">>",                  opts)
vim.keymap.set("n", "<A-h>", "<<",                  opts)
vim.keymap.set("i", "<A-j>", "<Esc>:m .+1<CR>==gi", opts)
vim.keymap.set("i", "<A-k>", "<Esc>:m .-2<CR>==gi", opts)
vim.keymap.set("v", "<A-j>", ":m '>+1<CR>gv=gv",    opts)
vim.keymap.set("v", "<A-k>", ":m '<-2<CR>gv=gv",    opts)
vim.keymap.set("v", "<A-h>", "1<<CR>gv",            opts)
vim.keymap.set("v", "<A-l>", "1><CR>gv",            opts)

-- Better UX
vim.keymap.set("n", "B", "^",   opts) -- Begining of the line
vim.keymap.set("n", "E", "$",   opts) -- End of the line
vim.keymap.set("n", "Y", "y$",  opts) -- Yank behave like like D, C, ...

-- Paste
vim.keymap.set("v", "<leader>p", '"_dP', opts) -- Replace selection with last yank

-- Yank
vim.keymap.set("n", "<leader>y", '"+y',     opts)
vim.keymap.set("v", "<leader>y", '"+y',     opts)
vim.keymap.set("n", "<leader>Y", 'gg"+yG',  opts)

-- Delete
vim.keymap.set("n", "<leader>d", '"_d', opts) -- blackhole delete
vim.keymap.set("v", "<leader>d", '"_d', opts) -- same but for visual mode

-- Move between pane
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

-- File tree
vim.keymap.set("n", "<C-n>", ":NvimTreeToggle<CR>", opts)
vim.keymap.set("n", "<C-f>", ":NvimTreeFindFile<CR>", opts)

-- Search
vim.keymap.set("n", "n", "nzzzv", opts) -- Next then center
vim.keymap.set("n", "N", "Nzzzv", opts) -- Previous then center
vim.keymap.set("n", "*", "/\\<<C-R>=expand('<cword>')<CR>\\><CR>", opts) -- search word under cursor
vim.keymap.set("n", "#", "?\\<<C-R>=expand('<cword>')<CR>\\><CR>", opts) -- same but backward


-- LocalList
vim.keymap.set("n", "<localleader>o", ":lopen<CR>",         opts)
vim.keymap.set("n", "<localleader>q", ":lclose<CR>",        opts)
vim.keymap.set("n", "<localleader>n", ":lnext<CR>zz",       opts)
vim.keymap.set("n", "<localleader>p", ":lprevious<CR>zz",   opts)

-- Quicklist
vim.keymap.set("n", "<leader>o", ":copen<CR>",          opts)
vim.keymap.set("n", "<leader>n", ":cnext<CR>zz",        opts)
vim.keymap.set("n", "<leader>p", ":cprevious<CR>zz",    opts)

-- Splits
vim.keymap.set("n", "<leader>v", ":vsplit<CR>",     opts)
vim.keymap.set("n", "<leader>h", ":split<CR>",      opts)
vim.keymap.set("n", "<leader>q", ":close<CR>",      opts)
vim.keymap.set("n", "<leader><leader>q", ":bd<CR>", opts)

-- Windows
vim.keymap.set("n", "=", ":vertical resize +5<CR>", opts)
vim.keymap.set("n", "-", ":vertical resize -5<CR>", opts)

-- Fugitive
vim.keymap.set("n", "<localleader>gg", ":Git<CR>",          opts)
vim.keymap.set("n", "<localleader>gd", ":Git diff<CR>",     opts)
vim.keymap.set("n", "<localleader>gb", ":Git blame<CR>",    opts)

-- Vim-test
vim.keymap.set("n", "<leader>tn", ":TestNearest<CR>", opts)
vim.keymap.set("n", "<leader>tf", ":TestFile<CR>", opts)
vim.keymap.set("n", "<leader>ts", ":TestSuite<CR>", opts)
vim.keymap.set("n", "<leader>tl", ":e last_test_results.log<CR>", opts)
vim.keymap.set("n", "<leader>tt", ":TestLast<CR>", opts)
vim.keymap.set("t", "<C-o>", "<C-\\><C-n>", opts)

-- Projectionist
vim.keymap.set("n", "<leader>ga",   ":A<CR>",   opts)
vim.keymap.set("n", "<leader>vga",  ":AV<CR>",  opts)
vim.keymap.set("n", "<leader>hga",  ":AS<CR>",  opts)

-- Telescope
vim.keymap.set("n", "<leader>*",  require('telescope.builtin').grep_string,             opts)
vim.keymap.set("n", "<leader>ss", require('telescope.builtin').git_files,               opts)
vim.keymap.set("n", "<leader>sf", require('telescope.builtin').live_grep,               opts)
vim.keymap.set("n", "<leader>sl", require('telescope.builtin').lsp_document_symbols,    opts)
vim.keymap.set("n", "<leader>st", require('telescope.builtin').treesitter,              opts)
vim.keymap.set("n", "<leader>sb", require('telescope.builtin').buffers,                 opts)
vim.keymap.set("n", "<leader>sg", require('telescope.builtin').git_status,              opts)

-- Harpoon
vim.keymap.set("n", "<leader>m", require('harpoon.ui').toggle_quick_menu,           opts)
vim.keymap.set("n", "<leader>a", require('harpoon.mark').add_file,                  opts)
vim.keymap.set("n", "<leader>1", "<CMD>lua require('harpoon.ui').nav_file(1)<CR>",  opts)
vim.keymap.set("n", "<leader>2", "<CMD>lua require('harpoon.ui').nav_file(2)<CR>",  opts)
vim.keymap.set("n", "<leader>3", "<CMD>lua require('harpoon.ui').nav_file(3)<CR>",  opts)
vim.keymap.set("n", "<leader>4", "<CMD>lua require('harpoon.ui').nav_file(4)<CR>",  opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer

local G = {}

G.on_attach = function(_, bufnr)
	--Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	local buff_opts = { noremap = true, silent = true, buffer = bufnr }

	-- See `:help vim.lsp.*` for documentation on any of the below functions
	vim.keymap.set("n", "gd", vim.lsp.buf.declaration, buff_opts)
	vim.keymap.set("n", "<leader>gd", vim.lsp.buf.definition, buff_opts)
	vim.keymap.set("n", "<leader>gr", vim.lsp.buf.references, buff_opts)
	vim.keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, buff_opts)
	vim.keymap.set("n", "K", vim.lsp.buf.hover, buff_opts)
	vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, buff_opts)
	vim.keymap.set("n", "<leader>rr", vim.lsp.buf.rename, buff_opts)
	vim.keymap.set("n", "<leader>ra", vim.lsp.buf.code_action, buff_opts)
end

return G
