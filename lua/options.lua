local o = vim.o

o.autowrite = true
o.autowriteall = true
o.hidden = true
o.title = true
o.clipboard = "unnamedplus"
o.completeopt = "menuone,noselect,noinsert"
o.cursorline = true
o.lazyredraw = false
o.laststatus = 3

o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
o.expandtab = true

o.swapfile = false
o.wrap = false

o.formatoptions = "cqrnj"
--  - "a" -- Auto formatting is BAD.
--  - "t" -- Don't auto format my code. I got linters for that.
--  + "c" -- In general, I like it when comments respect textwidth
--  + "q" -- Allow formatting comments w/ gq
--  - "o" -- O and o, don't continue comments
--  + "r" -- But do continue when pressing enter.
--  + "n" -- Indent past the formatlistpat, not underneath it.
--  + "j" -- Auto-remove comments if possible.
--  - "2" -- I'm not in gradeschool anymore
o.number = true
o.relativenumber = true
o.updatetime = 1000
o.timeoutlen = 400 -- faster timeout wait time
o.scrolloff = 8
o.pumheight = 5
o.colorcolumn = "120"

o.ignorecase = true
o.smartcase = true
o.incsearch = true
o.inccommand = "split"

o.splitbelow = true
o.splitright = true
o.termguicolors = true

o.showmode = false -- don't show mode
o.autoindent = true -- enable autoindent
o.smartindent = true -- smarter indentation
o.smarttab = true -- make tab behaviour smarter
o.equalalways = true -- make window size always equal

o.shortmess = "ocsa" -- disable some stuff on shortmess
o.signcolumn = "yes" -- always show sign column

o.undodir = vim.fn.stdpath("data") .. "/undo"
o.undofile = true
