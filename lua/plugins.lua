local disabled_built_ins = {
	"netrw",
	"netrwPlugin",
	"netrwSettings",
	"netrwFileHandlers",
	"gzip",
	"zip",
	"zipPlugin",
	"tar",
	"tarPlugin",
	"getscript",
	"getscriptPlugin",
	"vimball",
	"vimballPlugin",
	"2html_plugin",
	"logipat",
	"rrhelper",
	"spellfile_plugin",
	"matchit",
}

for _, plugin in pairs(disabled_built_ins) do
	vim.g["loaded_" .. plugin] = 1
end

vim.cmd([[packadd packer.nvim]])

return require("packer").startup(function(use)
	use("wbthomason/packer.nvim") -- Packer can manage itself

	-- General plugins
	use("tpope/vim-surround") -- Easy change for surrounding
	use("tpope/vim-fugitive") -- Support for git
	use("tpope/vim-commentary") -- Comment source code
	use("tpope/vim-repeat") -- Repeat commands from plugins
	use("tpope/vim-abolish") -- Allow to refactor camelCase to snake_case etc...
	use("tpope/vim-projectionist") -- Easy swap from code to test

	-- Better UI
	use("onsails/lspkind-nvim")
    use {
        "folke/trouble.nvim",
        requires = "kyazdani42/nvim-web-devicons",
        config = function()
            require("trouble").setup {
                mode="quickfix"
              -- your configuration comes here
              -- or leave it empty to use the default settings
              -- refer to the configuration section below
            }
        end
    }
	use("hoob3rt/lualine.nvim") -- Status bar
	use("kyazdani42/nvim-web-devicons") -- Nice file icons
	use({
		"kyazdani42/nvim-tree.lua", -- File tree view
		opt = true,
		requires = "kyazdani42/nvim-web-devicons",
		cmd = { "NvimTreeToggle", "NvimTreeFindFile" },
		config = function()
			require("nvim-tree").setup({
				filters = {
					dotfiles = true,
					custom = {
						".cache",
						".git",
						".vscode",
						"__init__.py",
						"__pycache__",
						"node_modules",
					},
				},
			})
		end,
	})
	use("lukas-reineke/indent-blankline.nvim") -- Display nice indent guide
	-- use("kriyss/nord.nvim") -- Custom Lua nord colorsheme
	-- use("shaunsingh/nord.nvim") -- Lua nord colorsheme
    use ("rmehri01/onenord.nvim")

	-- Better UX
	-- use("tversteeg/registers.nvim") -- Display registers content
    use {
        "folke/which-key.nvim",
        config = function()
            require("which-key").setup()
        end
    }

	use({
		"nvim-telescope/telescope.nvim",
		config = function()
			require("telescope").setup({
				defaults = {
					mappings = {
						i = {
							["<C-d>"] = require("telescope.actions").delete_buffer,
						},
						n = {
							["<C-d>"] = require("telescope.actions").delete_buffer,
						},
					},
				},
			})
		end,
		requires = { "nvim-lua/popup.nvim", "nvim-lua/plenary.nvim" },
	})
	use({ "ThePrimeagen/harpoon", requires = { "nvim-lua/popup.nvim", "nvim-lua/plenary.nvim" }})

	-- Code
	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })
	use("nvim-treesitter/nvim-treesitter-textobjects") -- Custom text object definition based on treesitter
	use("romgrk/nvim-treesitter-context")
	use("neovim/nvim-lspconfig") -- Add LSP config support
	use({
		"williamboman/mason.nvim",
		config = function()
			require("mason").setup()
		end,
	})
	use({
		"hrsh7th/nvim-cmp",
		requires = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
            "hrsh7th/cmp-nvim-lsp-signature-help",
			"L3MON4D3/LuaSnip",
			"saadparwaiz1/cmp_luasnip",
            "rafamadriz/friendly-snippets"
		},
		config = function()
			local cmp = require("cmp")
			local lspkind = require("lspkind")
			cmp.setup({
				formatting = {
					format = lspkind.cmp_format({
						with_text = true,
						menu = {
							buffer = "[BUF]",
							nvim_lsp = "[LSP]",
							luasnip = "[SNP]",
						},
					}),
				},
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},
				mapping = cmp.mapping.preset.insert({
					["<C-b>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.abort(),
					["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
				}),
				sources = cmp.config.sources({
					{ name = "nvim_lsp" },
					{ name = "buffer" },
					{ name = "luasnip" },
                    { name = 'nvim_lsp_signature_help' },
				}),
			})
		end,
	})

	use("dense-analysis/ale") -- Code analysis and auto fixers
	use("vim-test/vim-test") -- Launch tests from vim
end)
