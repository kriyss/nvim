let test#python#runner = 'djangotest'
lua require("luasnip.loaders.from_vscode").load({ include = { "python" } })

function! CustomDjangoSettingsWithParser(cmd) abort
    let l:settings = 'settings.settings.unit_test'
    return a:cmd . ' --settings=' . l:settings . ' 2>&1 >/dev/null | ptparser'
endfunction

function! CustomDjangoSettings(cmd) abort
    let l:settings = 'settings.settings.unit_test'
    return a:cmd . ' --settings=' . l:settings
endfunction

let g:test#custom_transformations = {'custom_settings': function('CustomDjangoSettings')}
let g:test#transformation = 'custom_settings'

ab throw raise
ab catch except

ab slef self
ab sefl self

ab retrun return
ab retunr return

ab salve slave

ab adebt # DEBT(SURE): [SURE-XXX]
ab anote # NOTE(SURE):
