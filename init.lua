local vim = vim
vim.g["ale_disable_lsp"] = 1

require("options")
require("plugins")
require("mappings")
require("code")
require("theme")
